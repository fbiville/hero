FROM golang:1.12 as build-env
ENV GO111MODULE=on
WORKDIR /go/src/app
COPY . .
RUN go get -d -v ./...
RUN go install -v

FROM gcr.io/distroless/base
COPY --from=build-env /go/bin/hero /
CMD ["/hero"]