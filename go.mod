module github.com/fbiville/hero

go 1.12

require (
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	golang.org/x/sys v0.0.0-20190801041406-cbf593c0f2f3 // indirect
)
