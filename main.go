package main

import (
	"fmt"
	"github.com/fbiville/hero/pkg/config"
	"github.com/fbiville/hero/pkg/okta"
	"github.com/fbiville/hero/pkg/slack"
	"os"
	"strings"
)

const SlackBaseUri = "https://slack.com"

func main() {
	configuration, err := config.LoadFromEnvironment()
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	newHeroSlackUser := configureInSlack(configuration)
	configureInOkta(configuration, newHeroSlackUser)
}

func configureInSlack(configuration *config.Config) *slack.User {
	slackToken := configuration.SlackToken
	slackTokenRepository := slack.NewTokenRepository(SlackBaseUri)
	slackGroupRepository := slack.NewGroupRepository(SlackBaseUri, slackToken)
	slackGroupMemberRepository := slack.NewGroupMemberRepository(SlackBaseUri, slackToken)
	slackUserRepository := slack.NewUserRepository(SlackBaseUri, slackToken)
	slackMessageRepository := slack.NewMessageRepository(SlackBaseUri, slackToken)

	validateSlackToken(slackTokenRepository, slackToken)
	slackGroups := findAllSlackGroups(slackGroupRepository)
	slackHeroGroup := findSlackHeroGroup(configuration.HeroGroupName, slackGroups)
	slackUsers := findSlackUsers(slackGroupMemberRepository, slackUserRepository, slackGroups, configuration)
	newHero := findNextHero(slackGroupMemberRepository, slackHeroGroup, slackUsers)
	registerNewHeroInSlackGroup(slackGroupMemberRepository, newHero, slackHeroGroup)
	sendSlackMessage(slackMessageRepository, &slack.Channel{Name: configuration.SlackChannel}, newHero)
	return newHero
}

func configureInOkta(configuration *config.Config, newHeroSlackUser *slack.User) {
	oktaToken := configuration.OktaToken
	if oktaToken == "" {
		fmt.Println("OKTA -- No action")
		os.Exit(0)
	}

	oktaUrl := configuration.OktaUrl
	newHeroName := newHeroSlackUser.Name

	oktaUserRepository := okta.NewUserRepository(oktaUrl, oktaToken)
	oktaGroupRepository := okta.NewGroupRepository(oktaUrl, oktaToken)
	oktaGroupMemberRepository := okta.NewGroupMemberRepository(oktaUrl, oktaToken)

	oktaHeroUser := findOktaUser(oktaUserRepository, newHeroName)
	oktaHeroGroup := findOktaHeroGroup(oktaGroupRepository, configuration.HeroGroupName)
	registerNewHeroInOktaGroup(oktaGroupMemberRepository, newHeroName, oktaHeroGroup, oktaHeroUser)
}

func validateSlackToken(tokenRepository slack.TokenRepository, token string) {
	fmt.Print("SLACK -- Validate token")

	tokenValid, err := tokenRepository.Validate(token)
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	if !tokenValid {
		printError(" ❌ invalid Slack token, aborting \n")
	}
	fmt.Println(" ✅")
}

func findAllSlackGroups(groupRepository slack.GroupRepository) []slack.Group {
	groups, err := groupRepository.FindAll()
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	return groups
}

func findSlackHeroGroup(groupName string, groups []slack.Group) *slack.Group {
	fmt.Printf("SLACK -- Searching for the hero group: %s ", groupName)

	result := findGroupByName(groups, groupName)
	fmt.Println(" ✅")
	return result
}

func findSlackUsers(
	groupMemberRepository slack.GroupMemberRepository,
	userRepository slack.UserRepository,
	groups []slack.Group,
	configuration *config.Config) []slack.User {

	var users []slack.User
	if configuration.OriginalGroupName != "" {
		users = findAllUsersInOriginalGroup(groupMemberRepository, groups, configuration.OriginalGroupName)
	} else {
		users = findSpecificUsers(userRepository, configuration.UsersName)
	}
	if len(users) < 2 {
		printError(" ❌ expected to retrieve at least two Slack users, found %d \n", len(users))
	}
	fmt.Println(" ✅")
	return users
}

func findNextHero(
	groupMemberRepository slack.GroupMemberRepository,
	heroGroup *slack.Group,
	users []slack.User) *slack.User {

	fmt.Print("SLACK -- Getting the current hero ")
	heroes, err := groupMemberRepository.FindAll(heroGroup)
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	if len(heroes) != 1 {
		printError(" ❌ expected exactly 1 Slack user in hero group, found %d", len(heroes))
	}
	currentHero := heroes[0]
	index := indexOfUser(users, currentHero)
	if index == -1 {
		printError(" ❌ current hero not in hero group, aborting \n")
	}
	fmt.Println(" ✅")
	return &users[(index+1)%len(users)]
}

func registerNewHeroInSlackGroup(groupMemberRepository slack.GroupMemberRepository, nextHero *slack.User, heroGroup *slack.Group) {
	fmt.Printf("SLACK -- Setting the new hero: %s ", nextHero.Name)

	err := groupMemberRepository.Add(heroGroup, nextHero)
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	fmt.Println(" ✅")
}

func sendSlackMessage(messageRepository slack.MessageRepository, slackChannel *slack.Channel, nextHero *slack.User) {
	fmt.Print("SLACK -- Sending message ")

	message := fmt.Sprintf("Everyone give a round of applause :clap: for the hero of the day: <@%s>", nextHero.Name)
	if err := messageRepository.Publish(slackChannel, message); err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	fmt.Println(" ✅")
}

func findOktaUser(oktaUserRepository okta.UserRepository, newHeroName string) *okta.User {
	fmt.Printf("OKTA -- Searching user: %s ", newHeroName)
	oktaUsers, err := oktaUserRepository.FindAllByName(newHeroName)
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	if len(oktaUsers) != 1 {
		printError(" ❌ expected exactly 1 OKTA user to have name %s, found %d", newHeroName, len(oktaUsers))
	}
	fmt.Println(" ✅")
	return &oktaUsers[0]
}

func findOktaHeroGroup(oktaGroupRepository okta.GroupRepository, heroGroupName string) *okta.Group {
	fmt.Printf("OKTA -- Searching for group: %s ", heroGroupName)
	oktaGroups, err := oktaGroupRepository.FindAllByName(heroGroupName)
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	if len(oktaGroups) != 1 {
		printError(" ❌ expected exactly 1 OKTA group to have name %s, found %d", heroGroupName, len(oktaGroups))
	}
	fmt.Println(" ✅")
	return &oktaGroups[0]
}

func registerNewHeroInOktaGroup(
	oktaGroupMemberRepository okta.GroupMemberRepository,
	newHeroName string,
	oktaHeroGroup *okta.Group,
	oktaHeroUser *okta.User) {

	fmt.Printf("OKTA -- Setting the new hero: %s ", newHeroName)
	oktaUsers, err := oktaGroupMemberRepository.FindAll(oktaHeroGroup)
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	for _, oktaUser := range oktaUsers {
		if err := oktaGroupMemberRepository.Remove(oktaHeroGroup, &oktaUser); err != nil {
			printError(" ❌ %s\n", err.Error())
		}
	}
	err = oktaGroupMemberRepository.Add(oktaHeroGroup, oktaHeroUser)
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	fmt.Println(" ✅")
}

func findAllUsersInOriginalGroup(
	groupMemberRepository slack.GroupMemberRepository,
	groups []slack.Group,
	groupName string) []slack.User {

	fmt.Printf("SLACK -- Getting users from group: %s ", groupName)

	originalGroup := findGroupByName(groups, groupName)
	users, err := groupMemberRepository.FindAll(originalGroup)
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	return users
}

func findSpecificUsers(userRepository slack.UserRepository, userNames string) []slack.User {
	fmt.Printf("SLACK -- Searching for users: %s ", userNames)
	allUsers, err := userRepository.FindAll()
	if err != nil {
		printError(" ❌ %s\n", err.Error())
	}
	var result []slack.User
	quotedUserNames := strings.Split(userNames, ",")
	for _, user := range allUsers {
		for _, name := range quotedUserNames {
			if user.Name == name {
				result = append(result, user)
			}
		}
	}
	return result
}

func findGroupByName(groups []slack.Group, groupName string) *slack.Group {
	group := matchSlackGroupByName(groups, groupName)
	if group == nil {
		panic(fmt.Sprint(" ❌ group not found, aborting \n"))
	}
	return group
}

func matchSlackGroupByName(groups []slack.Group, name string) *slack.Group {
	for _, group := range groups {
		if group.Name == name {
			return &group
		}
	}
	return nil
}

func indexOfUser(users []slack.User, currentHero slack.User) int {
	for i, user := range users {
		if user.Id == currentHero.Id {
			return i
		}
	}
	return -1
}

func printError(msg string, args ...interface{}) {
	_, _ = fmt.Fprintf(os.Stderr, msg, args...)
	os.Exit(1)
}
