package config

import (
	"fmt"
	"os"
)

type Config struct {
	SlackToken        string
	OktaToken         string
	OktaUrl           string
	HeroGroupName     string
	UsersName         string
	OriginalGroupName string
	SlackChannel      string
}

func LoadFromEnvironment() (*Config, error) {
	slackToken, err := tryMandatoryGet("SLACK_TOKEN")
	if err != nil {
		return nil, err
	}
	oktaToken := os.Getenv("OKTA_TOKEN")
	oktaUrl := os.Getenv("OKTA_URL")
	if oktaToken != "" && oktaUrl == "" {
		return nil, fmt.Errorf("missing environment variable: OKTA_URL must be set because OKTA_TOKEN is set")
	}

	heroGroupName, err := tryMandatoryGet("HERO_GROUP_NAME")
	if err != nil {
		return nil, err
	}
	usersName := os.Getenv("USERS_NAME")
	originalGroupName := os.Getenv("ORIGINAL_GROUP_NAME")
	if usersName == "" && originalGroupName == "" {
		return nil, fmt.Errorf("missing environment variable: either USERS_NAME or ORIGINAL_GROUP_NAME must be set (both missing)")
	}
	if usersName != "" && originalGroupName != "" {
		return nil, fmt.Errorf("conflicting environment variables: either USERS_NAME or ORIGINAL_GROUP_NAME must be set (both set)")
	}
	slackChannel, err := tryMandatoryGet("SLACK_CHANNEL")
	if err != nil {
		return nil, err
	}
	return &Config{
		SlackToken:        slackToken,
		OktaToken:         oktaToken,
		OktaUrl:           oktaUrl,
		HeroGroupName:     heroGroupName,
		UsersName:         usersName,
		OriginalGroupName: originalGroupName,
		SlackChannel:      slackChannel,
	}, nil
}

func tryMandatoryGet(envvar string) (string, error) {
	result := os.Getenv(envvar)
	if result == "" {
		return "", fmt.Errorf("missing environment variable %s", envvar)
	}
	return result, nil
}
