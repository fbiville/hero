package config_test

import (
	"fmt"
	"github.com/fbiville/hero/pkg/config"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	"os"
)

var _ = Describe("Configuration loader", func() {

	Describe("when looking up environment variables", func() {

		It("successfully loads with original group name", func() {
			expectedConfig := &config.Config{
				SlackToken:        "some-token",
				OktaToken:         "some-other-token",
				OktaUrl:           "some-url",
				HeroGroupName:     "some-group",
				OriginalGroupName: "some-other-group",
				SlackChannel:      "some-channel",
			}
			formerEnvironment := setUpEnvironment(map[string]string{
				"SLACK_TOKEN":         expectedConfig.SlackToken,
				"OKTA_TOKEN":          expectedConfig.OktaToken,
				"OKTA_URL":            expectedConfig.OktaUrl,
				"HERO_GROUP_NAME":     expectedConfig.HeroGroupName,
				"ORIGINAL_GROUP_NAME": expectedConfig.OriginalGroupName,
				"SLACK_CHANNEL":       expectedConfig.SlackChannel,
			})
			defer restoreEnvironment(formerEnvironment)

			config, err := config.LoadFromEnvironment()

			Expect(err).NotTo(HaveOccurred())
			Expect(config).To(Equal(expectedConfig))

		})

		It("successfully loads with users' name", func() {
			expectedConfig := &config.Config{
				SlackToken:    "some-token",
				HeroGroupName: "some-group",
				SlackChannel:  "some-channel",
				UsersName:     "some-user",
			}
			formerEnvironment := setUpEnvironment(map[string]string{
				"SLACK_TOKEN":     expectedConfig.SlackToken,
				"HERO_GROUP_NAME": expectedConfig.HeroGroupName,
				"SLACK_CHANNEL":   expectedConfig.SlackChannel,
				"USERS_NAME":      expectedConfig.UsersName,
			})
			defer restoreEnvironment(formerEnvironment)

			config, err := config.LoadFromEnvironment()

			Expect(err).NotTo(HaveOccurred())
			Expect(config).To(Equal(expectedConfig))
		})
	})

	DescribeTable("fails when environment variables are missing",
		func(variableName string, requiredEnvironment map[string]string) {
			formerEnvironment := setUpEnvironment(requiredEnvironment)
			defer restoreEnvironment(formerEnvironment)

			_, err := config.LoadFromEnvironment()

			Expect(err).To(MatchError(fmt.Sprintf("missing environment variable %s", variableName)))
		},
		Entry("missing SLACK_TOKEN",
			"SLACK_TOKEN",
			map[string]string{
				"HERO_GROUP_NAME": "some-group",
				"SLACK_CHANNEL":   "some-channel",
				"USERS_NAME":      "some-user"}),
		Entry("missing HERO_GROUP_NAME",
			"HERO_GROUP_NAME",
			map[string]string{
				"SLACK_TOKEN":   "some-token",
				"SLACK_CHANNEL": "some-channel",
				"USERS_NAME":    "some-user"}),
		Entry("missing SLACK_CHANNEL",
			"SLACK_CHANNEL",
			map[string]string{
				"SLACK_TOKEN":     "some-token",
				"HERO_GROUP_NAME": "some-group",
				"USERS_NAME":      "some-user"}),
	)

	It("fails if OKTA token is set but not OKTA URL is not", func() {
		formerEnvironment := setUpEnvironment(map[string]string{
			"SLACK_TOKEN":     "some-token",
			"OKTA_TOKEN":      "some-token",
			"HERO_GROUP_NAME": "some-group",
			"SLACK_CHANNEL":   "some-channel",
		})
		defer restoreEnvironment(formerEnvironment)

		_, err := config.LoadFromEnvironment()

		Expect(err).To(MatchError("missing environment variable: OKTA_URL must be set because OKTA_TOKEN is set"))
	})

	It("fails if both user names and original group name are set", func() {
		formerEnvironment := setUpEnvironment(map[string]string{
			"SLACK_TOKEN":         "some-token",
			"OKTA_TOKEN":          "some-token",
			"OKTA_URL":            "some-url",
			"HERO_GROUP_NAME":     "some-group",
			"SLACK_CHANNEL":       "some-channel",
			"USERS_NAME":          "some-user",
			"ORIGINAL_GROUP_NAME": "some-other-group",
		})
		defer restoreEnvironment(formerEnvironment)

		_, err := config.LoadFromEnvironment()

		Expect(err).To(MatchError("conflicting environment variables: either USERS_NAME or ORIGINAL_GROUP_NAME must be set (both set)"))
	})
})

func setUpEnvironment(envVars map[string]string) map[string]string {
	formerEnvironment := make(map[string]string)
	for key, value := range envVars {
		formerEnvironment[key] = os.Getenv(key)
		err := os.Setenv(key, value)
		if err != nil {
			panic(err)
		}
	}
	return formerEnvironment
}

func restoreEnvironment(envVars map[string]string) {
	for key, value := range envVars {
		err := os.Setenv(key, value)
		if err != nil {
			panic(err)
		}
	}
}
