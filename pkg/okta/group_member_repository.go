package okta

import (
	"fmt"
	"net/http"
)

type GroupMemberRepository interface {
	FindAll(group *Group) ([]User, error)
	Remove(group *Group, user *User) error
	Add(group *Group, user *User) error
}

type groupMemberHttpRepository struct {
	serverAddress string
	token         string
	httpClient    *http.Client
}

func NewGroupMemberRepository(serverAddress string, token string) GroupMemberRepository {
	return &groupMemberHttpRepository{
		serverAddress: serverAddress,
		token:         token,
		httpClient:    &http.Client{},
	}
}

func (repo *groupMemberHttpRepository) FindAll(group *Group) ([]User, error) {
	getUri := fmt.Sprintf("%s/api/v1/groups/%d/users", repo.serverAddress, group.Id)
	request, err := newJsonAuthenticatedRequest("GET", getUri, repo.token)
	if err != nil {
		return nil, err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return nil, fmt.Errorf("[OKTA client] unable to find users in group %d - server responded with %d (expected 200)", group.Id, statusCode)
	}
	responseBodyReader := response.Body
	defer func() {
		err := responseBodyReader.Close()
		if err != nil {
			_ = fmt.Errorf("[OKTA client] cannot close response body: %v", err)
		}
	}()
	return unmarshallUsers(responseBodyReader)
}

func (repo *groupMemberHttpRepository) Remove(group *Group, user *User) error {
	saveUri := fmt.Sprintf("%s/api/v1/groups/%d/users/%d", repo.serverAddress, group.Id, user.Id)
	request, err := newJsonAuthenticatedRequest("DELETE", saveUri, repo.token)
	if err != nil {
		return err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return err
	}

	statusCode := response.StatusCode
	if statusCode != 204 {
		return fmt.Errorf("[OKTA client] unable to remove user %d from group %d - server responded with %d (expected 204)", user.Id, group.Id, statusCode)
	}
	return nil
}

func (repo *groupMemberHttpRepository) Add(group *Group, user *User) error {
	saveUri := fmt.Sprintf("%s/api/v1/groups/%d/users/%d", repo.serverAddress, group.Id, user.Id)
	request, err := newJsonAuthenticatedRequest("PUT", saveUri, repo.token)
	if err != nil {
		return err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return err
	}

	statusCode := response.StatusCode
	if statusCode != 204 {
		return fmt.Errorf("[OKTA client] unable to add user %d to group %d - server responded with %d (expected 204)", user.Id, group.Id, statusCode)
	}
	return nil
}
