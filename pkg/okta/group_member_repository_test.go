package okta_test

import (
	"fmt"
	"github.com/fbiville/hero/pkg/okta"
	"github.com/fbiville/hero/pkg/test_support/http"
	"github.com/fbiville/hero/pkg/test_support/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Okta group member repository", func() {

	var server http.HttpServer

	BeforeEach(func() {
		server = http.NewServer("127.0.0.1:0")
	})

	Describe("when finding members in a group", func() {

		groupId := 42

		It("succeeds", func() {
			expectedUsers := []okta.User{{Id: 1}, {Id: 2}, {Id: 2}}
			go func() {
				httpResponseBody := json.MarshallToJson(expectedUsers)
				server.Serve(okta.WithMemberGetRequestHandler(groupId, httpResponseBody))
			}()
			groupMemberRepository := okta.NewGroupMemberRepository(server.GetAddress(), "some-token")

			users, err := groupMemberRepository.FindAll(&okta.Group{Id: groupId})

			Expect(err).NotTo(HaveOccurred())
			Expect(users).To(Equal(expectedUsers))
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(http.WithStatusCode(500))
			}()
			groupMemberRepository := okta.NewGroupMemberRepository(server.GetAddress(), "some-token")

			_, err := groupMemberRepository.FindAll(&okta.Group{Id: groupId})

			Expect(err).To(MatchError(fmt.Sprintf("[OKTA client] unable to find users in group %d - server responded with 500 (expected 200)", groupId)))
		})
	})

	Describe("when removing a member from a group", func() {
		groupId := 42
		userId := 4242

		It("succeeds", func() {
			go func() {
				server.Serve(okta.WithMemberDeleteRequestHandler(groupId, userId))
			}()
			groupMemberRepository := okta.NewGroupMemberRepository(server.GetAddress(), "some-token")

			err := groupMemberRepository.Remove(&okta.Group{Id: groupId}, &okta.User{Id: userId})

			Expect(err).NotTo(HaveOccurred())
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(http.WithStatusCode(500))
			}()
			groupMemberRepository := okta.NewGroupMemberRepository(server.GetAddress(), "some-token")

			err := groupMemberRepository.Remove(&okta.Group{Id: groupId}, &okta.User{Id: userId})

			Expect(err).To(MatchError(fmt.Sprintf("[OKTA client] unable to remove user %d from group %d - server responded with 500 (expected 204)", userId, groupId)))
		})
	})

	Describe("when adding a member to a group", func() {
		groupId := 42
		userId := 4242

		It("succeeds", func() {
			go func() {
				server.Serve(okta.WithMemberPutRequestHandler(groupId, userId))
			}()
			groupMemberRepository := okta.NewGroupMemberRepository(server.GetAddress(), "some-token")

			err := groupMemberRepository.Add(&okta.Group{Id: groupId}, &okta.User{Id: userId})

			Expect(err).NotTo(HaveOccurred())
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(http.WithStatusCode(500))
			}()
			groupMemberRepository := okta.NewGroupMemberRepository(server.GetAddress(), "some-token")

			err := groupMemberRepository.Add(&okta.Group{Id: groupId}, &okta.User{Id: userId})

			Expect(err).To(MatchError(fmt.Sprintf("[OKTA client] unable to add user %d to group %d - server responded with 500 (expected 204)", userId, groupId)))
		})
	})

})
