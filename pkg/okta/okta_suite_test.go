package okta_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestOktaIntegration(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Okta Suite")
}
