package okta_test

import (
	"fmt"
	"github.com/fbiville/hero/pkg/okta"
	"github.com/fbiville/hero/pkg/test_support/http"
	"github.com/fbiville/hero/pkg/test_support/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Okta user repository", func() {
	var server http.HttpServer

	BeforeEach(func() {
		server = http.NewServer("127.0.0.1:0")
	})

	Describe("when finding user by name", func() {
		userName := "some-user"

		It("succeeds", func() {
			expectedUsers := []okta.User{{Id: 1}, {Id: 2}, {Id: 2}}
			go func() {
				httpResponseBody := json.MarshallToJson(expectedUsers)
				server.Serve(okta.WithUserGetRequestHandler(userName, httpResponseBody))
			}()
			userRepository := okta.NewUserRepository(server.GetAddress(), "some-token")

			users, err := userRepository.FindAllByName(userName)

			Expect(err).NotTo(HaveOccurred())
			Expect(users).To(Equal(expectedUsers))
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(http.WithStatusCode(500))
			}()
			userRepository := okta.NewUserRepository(server.GetAddress(), "some-token")

			_, err := userRepository.FindAllByName(userName)

			Expect(err).To(MatchError(fmt.Sprintf("[OKTA client] unable to find user named %s - server responded with 500 (expected 200)", userName)))
		})
	})
})
