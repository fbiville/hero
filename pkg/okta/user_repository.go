package okta

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

type User struct {
	Id int `json:"id"`
}

type UserRepository interface {
	FindAllByName(name string) ([]User, error)
}

type userHttpRepository struct {
	serverAddress string
	token         string
	httpClient    *http.Client
}

func NewUserRepository(serverAddress string, token string) UserRepository {
	return &userHttpRepository{
		serverAddress: serverAddress,
		token:         token,
		httpClient:    &http.Client{},
	}
}

func (repo *userHttpRepository) FindAllByName(name string) ([]User, error) {
	getUri := fmt.Sprintf("%s/api/v1/users", repo.serverAddress)
	request, err := newJsonAuthenticatedRequest("GET", getUri, repo.token)
	if err != nil {
		return nil, err
	}
	request.URL.RawQuery = newQueryString("q", fmt.Sprintf("%s@", name))

	response, err := repo.httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return nil, fmt.Errorf("[OKTA client] unable to find user named %s - server responded with %d (expected 200)", name, statusCode)
	}
	responseBodyReader := response.Body
	defer func() {
		err := responseBodyReader.Close()
		if err != nil {
			_ = fmt.Errorf("[OKTA client] cannot close response body: %v", err)
		}
	}()

	return unmarshallUsers(responseBodyReader)
}

func unmarshallUsers(responseBodyReader io.ReadCloser) ([]User, error) {
	responseBody, err := ioutil.ReadAll(responseBodyReader)
	if err != nil {
		return nil, err
	}
	var users []User
	err = json.Unmarshal(responseBody, &users)
	if err != nil {
		return nil, err
	}
	return users, nil
}
