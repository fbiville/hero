package okta

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

func newJsonAuthenticatedRequest(method, uri, token string) (*http.Request, error) {
	request, err := http.NewRequest(method, uri, strings.NewReader(""))
	if err != nil {
		return nil, err
	}
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", fmt.Sprintf("SSWS %s", token))
	return request, nil
}

func newQueryString(name, value string) string {
	return url.Values{name: []string{value}}.Encode()
}
