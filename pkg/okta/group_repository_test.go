package okta_test

import (
	"fmt"
	"github.com/fbiville/hero/pkg/okta"
	"github.com/fbiville/hero/pkg/test_support/http"
	"github.com/fbiville/hero/pkg/test_support/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Okta group repository", func() {
	var server http.HttpServer

	BeforeEach(func() {
		server = http.NewServer("127.0.0.1:0")
	})

	Describe("when finding group by name", func() {
		groupName := "some-group"

		It("succeeds", func() {
			expectedGroups := []okta.Group{{Id: 1}, {Id: 2}, {Id: 2}}
			go func() {
				httpResponseBody := json.MarshallToJson(expectedGroups)
				server.Serve(okta.WithGroupGetRequestHandler(groupName, httpResponseBody))
			}()
			groupRepository := okta.NewGroupRepository(server.GetAddress(), "some-token")

			groups, err := groupRepository.FindAllByName(groupName)

			Expect(err).NotTo(HaveOccurred())
			Expect(groups).To(Equal(expectedGroups))
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(http.WithStatusCode(500))
			}()
			groupRepository := okta.NewGroupRepository(server.GetAddress(), "some-token")

			_, err := groupRepository.FindAllByName(groupName)

			Expect(err).To(MatchError(fmt.Sprintf("[OKTA client] unable to find group named %s - server responded with 500 (expected 200)", groupName)))
		})
	})
})
