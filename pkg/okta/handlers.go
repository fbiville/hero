package okta

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

func WithMemberPutRequestHandler(expectedGroupId, expectedUserId int) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/v1/groups/%d/users/%d", expectedGroupId, expectedUserId)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "PUT" {
			responseWriter.WriteHeader(405)
			return
		}

		responseWriter.WriteHeader(204)
	}
}

func WithMemberDeleteRequestHandler(expectedGroupId, expectedUserId int) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/v1/groups/%d/users/%d", expectedGroupId, expectedUserId)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "DELETE" {
			responseWriter.WriteHeader(405)
			return
		}

		responseWriter.WriteHeader(204)
	}
}

func WithMemberGetRequestHandler(expectedGroupId int, jsonResponseBody []byte) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/v1/groups/%d/users", expectedGroupId)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "GET" {
			responseWriter.WriteHeader(405)
			return
		}

		responseWriter.WriteHeader(200)
		responseWriter.Header().Set("Content-Type", "application/json")
		_, err := responseWriter.Write(jsonResponseBody)
		if err != nil {
			panic(err)
		}
	}
}

func WithGroupGetRequestHandler(expectedGroupName string, jsonResponseBody []byte) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/v1/groups?q=%s", expectedGroupName)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "GET" {
			responseWriter.WriteHeader(405)
			return
		}

		responseWriter.WriteHeader(200)
		responseWriter.Header().Set("Content-Type", "application/json")
		_, err := responseWriter.Write(jsonResponseBody)
		if err != nil {
			panic(err)
		}
	}
}

func WithUserGetRequestHandler(expectedUserName string, jsonResponseBody []byte) http.HandlerFunc {
	nameInQueryString := url.QueryEscape(fmt.Sprintf("%s@", expectedUserName))
	expectedUri := fmt.Sprintf("/api/v1/users?q=%s", nameInQueryString)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "GET" {
			responseWriter.WriteHeader(405)
			return
		}

		responseWriter.WriteHeader(200)
		responseWriter.Header().Set("Content-Type", "application/json")
		_, err := responseWriter.Write(jsonResponseBody)
		if err != nil {
			panic(err)
		}
	}
}

func matchesCommonExpectations(request *http.Request, expectedUri string, responseWriter http.ResponseWriter) bool {
	if request.RequestURI != expectedUri {
		responseWriter.WriteHeader(404)
		return false
	}
	auth := request.Header.Get("Authorization")
	if !strings.HasPrefix(auth, "SSWS ") {
		responseWriter.WriteHeader(401)
		return false
	}
	contentType := request.Header.Get("Content-Type")
	if contentType != "application/json" {
		responseWriter.WriteHeader(415)
		return false
	}
	accept := request.Header.Get("Accept")
	if accept != "application/json" {
		responseWriter.WriteHeader(406)
		return false
	}
	return true
}
