package okta

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

type Group struct {
	Id int `json:"id"`
}

type GroupRepository interface {
	FindAllByName(name string) ([]Group, error)
}

type groupHttpRepository struct {
	serverAddress string
	token         string
	httpClient    *http.Client
}

func NewGroupRepository(serverAddress string, token string) GroupRepository {
	return &groupHttpRepository{
		serverAddress: serverAddress,
		token:         token,
		httpClient:    &http.Client{},
	}
}

func (repo *groupHttpRepository) FindAllByName(name string) ([]Group, error) {
	getUri := fmt.Sprintf("%s/api/v1/groups", repo.serverAddress)
	request, err := newJsonAuthenticatedRequest("GET", getUri, repo.token)
	if err != nil {
		return nil, err
	}
	request.URL.RawQuery = newQueryString("q", name)

	response, err := repo.httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return nil, fmt.Errorf("[OKTA client] unable to find group named %s - server responded with %d (expected 200)", name, statusCode)
	}
	responseBodyReader := response.Body
	defer func() {
		err := responseBodyReader.Close()
		if err != nil {
			_ = fmt.Errorf("[OKTA client] cannot close response body: %v", err)
		}
	}()

	return unmarshallGroups(responseBodyReader)
}

func unmarshallGroups(responseBodyReader io.ReadCloser) ([]Group, error) {
	responseBody, err := ioutil.ReadAll(responseBodyReader)
	if err != nil {
		return nil, err
	}
	var groups []Group
	err = json.Unmarshal(responseBody, &groups)
	if err != nil {
		return nil, err
	}
	return groups, nil
}
