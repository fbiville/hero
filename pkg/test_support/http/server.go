package http

import (
	"fmt"
	"net"
	"net/http"
)

type HttpServer interface {
	Serve(request http.HandlerFunc)
	GetAddress() string
}

type server struct {
	listener net.Listener
}

func NewServer(address string) HttpServer {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		panic(err)
	}
	return &server{listener: listener}
}

func (server *server) Serve(request http.HandlerFunc) {
	err := http.Serve(server.listener, request)
	if err != nil {
		panic(err)
	}
}

func (server *server) GetAddress() string {
	return fmt.Sprintf("http://%s", server.listener.Addr().String())
}
