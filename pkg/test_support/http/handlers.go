package http

import "net/http"

func WithStatusCode(statusCode int) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(statusCode)
	}
}