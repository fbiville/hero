package slack_test

import (
	"fmt"
	"github.com/fbiville/hero/pkg/slack"
	. "github.com/fbiville/hero/pkg/test_support/http"
	. "github.com/fbiville/hero/pkg/test_support/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Slack group member repository", func() {

	groupId := 42
	token := "some-token"
	var server HttpServer

	BeforeEach(func() {
		server = NewServer("127.0.0.1:0")
	})

	Describe("when finding all members", func() {

		It("succeeds", func() {
			expectedUsers := []slack.User{{Id: 1}, {Id: 2}, {Id: 3}}
			go func() {
				httpResponseBody := MarshallToJson(&slack.FindAllGroupMembersResponse{Users: expectedUsers})
				server.Serve(slack.WithGroupMemberGetRequestHandler(token, groupId, httpResponseBody))
			}()
			groupMemberRepository := slack.NewGroupMemberRepository(server.GetAddress(), token)

			members, err := groupMemberRepository.FindAll(&slack.Group{Id: groupId})

			Expect(err).NotTo(HaveOccurred())
			Expect(members).To(Equal(expectedUsers))
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(WithStatusCode(500))
			}()
			groupMemberRepository := slack.NewGroupMemberRepository(server.GetAddress(), token)

			_, err := groupMemberRepository.FindAll(&slack.Group{Id: groupId})

			Expect(err).To(MatchError(fmt.Sprintf("[Slack client] unable to find users in group %d - server responded with 500 (expected 200)", groupId)))
		})
	})

	Describe("when adding a user to a group", func() {

		userId := 4242

		It("succeeds", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.ResponseStatus{Ok: true})
				server.Serve(slack.WithGroupMemberPostRequestHandler(token, groupId, userId, httpResponseBody))
			}()
			groupMemberRepository := slack.NewGroupMemberRepository(server.GetAddress(), token)

			err := groupMemberRepository.Add(&slack.Group{Id: groupId}, &slack.User{Id: userId})

			Expect(err).NotTo(HaveOccurred())
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(WithStatusCode(500))
			}()
			groupMemberRepository := slack.NewGroupMemberRepository(server.GetAddress(), token)

			err := groupMemberRepository.Add(&slack.Group{Id: groupId}, &slack.User{Id: userId})

			Expect(err).To(MatchError(fmt.Sprintf("[Slack client] unable to add user %d to group %d - server responded with 500 (expected 200)", userId, groupId)))
		})

		It("fails if the embedded status is not OK", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.ResponseStatus{Ok: false})
				server.Serve(slack.WithGroupMemberPostRequestHandler(token, groupId, userId, httpResponseBody))
			}()
			groupMemberRepository := slack.NewGroupMemberRepository(server.GetAddress(), token)

			err := groupMemberRepository.Add(&slack.Group{Id: groupId}, &slack.User{Id: userId})

			Expect(err).To(MatchError(fmt.Sprintf("[Slack client] unable to add user %d to group %d - server responded with 200 but JSON status is not OK", userId, groupId)))
		})
	})
})
