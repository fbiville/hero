package slack

import (
	"fmt"
	"net/http"
	"net/url"
)

type Channel struct {
	Name string
}

type ResponseStatus struct {
	Ok bool `json:"ok"`
}

type MessageRepository interface {
	Publish(channel *Channel, message string) error
}

type messageHttpRepository struct {
	serverAddress string
	token         string
	httpClient    *http.Client
}

func NewMessageRepository(serverAddress string, token string) MessageRepository {
	return &messageHttpRepository{
		serverAddress: serverAddress,
		token:         token,
		httpClient:    &http.Client{},
	}
}

func (repo *messageHttpRepository) Publish(channel *Channel, message string) error {
	request, err := newJsonRequest("POST", repo.publishMessageUri(channel, message))
	if err != nil {
		return err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return fmt.Errorf("[Slack client] unable to post message to channel %s - server responded with %d (expected 200)", channel.Name, statusCode)
	}
	responseStatus, err := unmarshallResponseStatus(response.Body)
	if err != nil {
		return err
	}
	if !responseStatus.Ok {
		return fmt.Errorf("[Slack client] unable to post message to channel %s - server responded with 200 but JSON status is not OK", channel.Name)
	}
	return nil
}

func (repo *messageHttpRepository) publishMessageUri(channel *Channel, message string) string {
	queryString := url.Values{
		"as_user": []string{"true"},
		"channel": []string{channel.Name},
		"text":    []string{message},
		"token":   []string{repo.token},
	}.Encode()

	return fmt.Sprintf("%s/api/chat.postMessage?%s", repo.serverAddress, queryString)
}
