package slack_test

import (
	"fmt"
	"github.com/fbiville/hero/pkg/slack"
	. "github.com/fbiville/hero/pkg/test_support/http"
	. "github.com/fbiville/hero/pkg/test_support/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Slack user repository", func() {

	var server HttpServer

	BeforeEach(func() {
		server = NewServer("127.0.0.1:0")
	})

	Describe("when finding all users", func() {

		expectedUsers := []slack.User{
			{Id: 1, Name: "John Doe"},
			{Id: 2, Name: "Jane Doe"},
		}
		token := "some-token"

		It("succeeds", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.FindAllUsersResponse{Members: expectedUsers})
				server.Serve(slack.WithUsersGetRequestHandler(token, httpResponseBody))
			}()
			userRepository := slack.NewUserRepository(server.GetAddress(), token)

			users, err := userRepository.FindAll()

			Expect(err).NotTo(HaveOccurred())
			Expect(users).To(Equal(expectedUsers))
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(WithStatusCode(500))
			}()
			userRepository := slack.NewUserRepository(server.GetAddress(), token)

			_, err := userRepository.FindAll()

			Expect(err).To(MatchError("[Slack client] unable to find all users - server responded with 500 (expected 200)"))
		})
	})

	Describe("when finding a user by their ID", func() {

		expectedUser := slack.User{Id: 4242, Name: "Jane Doe"}
		token := "some-token"

		It("succeeds", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.FindUserByIdResponse{User: &expectedUser})
				server.Serve(slack.WithUserGetRequestHandler(token, expectedUser.Id, httpResponseBody))
			}()
			userRepository := slack.NewUserRepository(server.GetAddress(), token)

			user, err := userRepository.FindById(expectedUser.Id)

			Expect(err).NotTo(HaveOccurred())
			Expect(user).To(Equal(&expectedUser))
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(WithStatusCode(500))
			}()
			userRepository := slack.NewUserRepository(server.GetAddress(), token)

			_, err := userRepository.FindById(expectedUser.Id)

			Expect(err).To(MatchError(fmt.Sprintf("[Slack client] unable to find user by id %d - server responded with 500 (expected 200)", expectedUser.Id)))
		})
	})
})
