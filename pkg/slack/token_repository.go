package slack

import (
	"fmt"
	"net/http"
	"net/url"
)

type TokenRepository interface {
	Validate(token string) (bool, error)
}

type tokenHttpRepository struct {
	serverAddress string
	httpClient    *http.Client
}

func NewTokenRepository(serverAddress string) TokenRepository {
	return &tokenHttpRepository{
		serverAddress: serverAddress,
		httpClient:    &http.Client{},
	}
}

func (repo tokenHttpRepository) Validate(token string) (bool, error) {
	request, err := newJsonRequest("GET", repo.validateTokenUri(token))
	if err != nil {
		return false, err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return false, err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return false, fmt.Errorf("[Slack client] unable to validate token %s - server responded with %d (expected 200)", token, statusCode)
	}
	responseStatus, err := unmarshallResponseStatus(response.Body)
	if err != nil {
		return false, err
	}
	return responseStatus.Ok, nil
}

func (repo tokenHttpRepository) validateTokenUri(token string) string {
	queryString := url.Values{
		"token":   []string{token},
	}.Encode()

	return fmt.Sprintf("%s/api/auth.test?%s", repo.serverAddress, queryString)
}
