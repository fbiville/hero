package slack

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type FindUserByIdResponse struct {
	User *User `json:"user"`
}

type FindAllUsersResponse struct {
	Members []User `json:"members"`
}

type UserRepository interface {
	FindAll() ([]User, error)
	FindById(userId int) (*User, error)
}

type userHttpRepository struct {
	serverAddress string
	token         string
	httpClient    *http.Client
}

func NewUserRepository(serverAddress string, token string) UserRepository {
	return &userHttpRepository{
		serverAddress: serverAddress,
		token:         token,
		httpClient:    &http.Client{},
	}
}

func (repo *userHttpRepository) FindAll() ([]User, error) {
	request, err := newJsonRequest("GET", repo.findAllUsersUri())
	if err != nil {
		return nil, err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return nil, fmt.Errorf("[Slack client] unable to find all users - server responded with %d (expected 200)", statusCode)
	}
	data, err := readAll(response.Body)
	if err != nil {
		return nil, err
	}
	return repo.unmarshallUsers(data)
}

func (repo *userHttpRepository) FindById(userId int) (*User, error) {
	request, err := newJsonRequest("GET", repo.findUserByIdUri(userId))
	if err != nil {
		return nil, err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return nil, fmt.Errorf("[Slack client] unable to find user by id %d - server responded with %d (expected 200)", userId, statusCode)
	}
	data, err := readAll(response.Body)
	if err != nil {
		return nil, err
	}
	return repo.unmarshallUser(data)
}

func (repo *userHttpRepository) findAllUsersUri() string {
	queryString := url.Values{
		"token": []string{repo.token},
	}.Encode()

	return fmt.Sprintf("%s/api/users.list?%s", repo.serverAddress, queryString)
}

func (repo *userHttpRepository) findUserByIdUri(userId int) string {
	queryString := url.Values{
		"token": []string{repo.token},
		"user":  []string{fmt.Sprintf("%d", userId)},
	}.Encode()

	return fmt.Sprintf("%s/api/users.info?%s", repo.serverAddress, queryString)
}

func (repo *userHttpRepository) unmarshallUser(data []byte) (*User, error) {
	userResponse := FindUserByIdResponse{}
	err := json.Unmarshal(data, &userResponse)
	if err != nil {
		return nil, err
	}
	return userResponse.User, nil
}

func (repo *userHttpRepository) unmarshallUsers(data []byte) ([]User, error) {
	userResponse := FindAllUsersResponse{}
	err := json.Unmarshal(data, &userResponse)
	if err != nil {
		return nil, err
	}
	return userResponse.Members, nil
}
