package slack_test

import (
	"fmt"
	"github.com/fbiville/hero/pkg/slack"
	. "github.com/fbiville/hero/pkg/test_support/http"
	. "github.com/fbiville/hero/pkg/test_support/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Slack message repository", func() {

	var server HttpServer

	BeforeEach(func() {
		server = NewServer("127.0.0.1:0")
	})

	Describe("when posting a message to a channel", func() {

		channelName := "some-channel"
		message := "hello world"
		token := "some-token"

		It("succeeds", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.ResponseStatus{Ok: true})
				server.Serve(slack.WithMessagePostRequestHandler(token, channelName, message, httpResponseBody))
			}()
			messageRepository := slack.NewMessageRepository(server.GetAddress(), token)

			err := messageRepository.Publish(&slack.Channel{Name: channelName}, message)

			Expect(err).NotTo(HaveOccurred())
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(WithStatusCode(500))
			}()
			messageRepository := slack.NewMessageRepository(server.GetAddress(), token)

			err := messageRepository.Publish(&slack.Channel{Name: channelName}, message)

			Expect(err).To(MatchError(fmt.Sprintf("[Slack client] unable to post message to channel %s - server responded with 500 (expected 200)", channelName)))
		})

		It("fails if the embedded status is not OK", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.ResponseStatus{Ok: false})
				server.Serve(slack.WithMessagePostRequestHandler(token, channelName, message, httpResponseBody))
			}()
			messageRepository := slack.NewMessageRepository(server.GetAddress(), token)

			err := messageRepository.Publish(&slack.Channel{Name: channelName}, message)

			Expect(err).To(MatchError(fmt.Sprintf("[Slack client] unable to post message to channel %s - server responded with 200 but JSON status is not OK", channelName)))
		})
	})
})
