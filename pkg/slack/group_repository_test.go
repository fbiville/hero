package slack_test

import (
	"github.com/fbiville/hero/pkg/slack"
	. "github.com/fbiville/hero/pkg/test_support/http"
	. "github.com/fbiville/hero/pkg/test_support/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Slack group repository", func() {

	var server HttpServer

	BeforeEach(func() {
		server = NewServer("127.0.0.1:0")
	})

	Describe("when finding all groups", func() {

		expectedGroups := []slack.Group{
			{Id: 1, Name: "Devs"},
			{Id: 2, Name: "Ops"},
		}
		token := "some-token"

		It("succeeds", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.FindAllGroupsResponse{Groups: expectedGroups})
				server.Serve(slack.WithGroupsGetRequestHandler(token, httpResponseBody))
			}()
			groupRepository := slack.NewGroupRepository(server.GetAddress(), token)

			groups, err := groupRepository.FindAll()

			Expect(err).NotTo(HaveOccurred())
			Expect(groups).To(Equal(expectedGroups))
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(WithStatusCode(500))
			}()
			groupRepository := slack.NewGroupRepository(server.GetAddress(), token)

			_, err := groupRepository.FindAll()

			Expect(err).To(MatchError("[Slack client] unable to find all groups - server responded with 500 (expected 200)"))
		})
	})
})
