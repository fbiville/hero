package slack_test

import (
	"fmt"
	"github.com/fbiville/hero/pkg/slack"
	. "github.com/fbiville/hero/pkg/test_support/http"
	. "github.com/fbiville/hero/pkg/test_support/json"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Slack token repository", func() {

	var server HttpServer

	BeforeEach(func() {
		server = NewServer("127.0.0.1:0")
	})

	Describe("when validating a token", func() {

		token := "some-token"

		It("succeeds", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.ResponseStatus{Ok: true})
				server.Serve(slack.WithTokenGetRequestHandler(token, httpResponseBody))
			}()
			tokenRepository := slack.NewTokenRepository(server.GetAddress())

			ok, err := tokenRepository.Validate(token)

			Expect(err).NotTo(HaveOccurred())
			Expect(ok).To(BeTrue())
		})

		It("fails if the server does not respond as expected", func() {
			go func() {
				server.Serve(WithStatusCode(500))
			}()
			tokenRepository := slack.NewTokenRepository(server.GetAddress())

			_, err := tokenRepository.Validate(token)

			Expect(err).To(MatchError(fmt.Sprintf("[Slack client] unable to validate token %s - server responded with 500 (expected 200)", token)))
		})

		It("fails if the embedded status is not OK", func() {
			go func() {
				httpResponseBody := MarshallToJson(&slack.ResponseStatus{Ok: false})
				server.Serve(slack.WithTokenGetRequestHandler(token, httpResponseBody))
			}()
			tokenRepository := slack.NewTokenRepository(server.GetAddress())

			ok, err := tokenRepository.Validate(token)

			Expect(err).NotTo(HaveOccurred())
			Expect(ok).To(BeFalse())
		})
	})
})
