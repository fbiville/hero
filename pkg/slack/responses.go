package slack

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
)

func unmarshallResponseStatus(responseBodyReader io.ReadCloser) (*ResponseStatus, error) {
	responseBody, err := readAll(responseBodyReader)
	if err != nil {
		return nil, err
	}
	var status ResponseStatus
	err = json.Unmarshal(responseBody, &status)
	if err != nil {
		return nil, err
	}
	return &status, nil
}

func readAll(responseBodyReader io.ReadCloser) ([]byte, error) {
	defer func() {
		err := responseBodyReader.Close()
		if err != nil {
			_ = fmt.Errorf("[Slack client] cannot close response body: %v", err)
		}
	}()
	return ioutil.ReadAll(responseBodyReader)
}