package slack

import (
	"fmt"
	"net/http"
	"net/url"
)

func WithMessagePostRequestHandler(token, channelName, message string, jsonResponseBody []byte) http.HandlerFunc {
	encodedMessage := url.QueryEscape(message)
	expectedUri := fmt.Sprintf("/api/chat.postMessage?as_user=true&channel=%s&text=%s&token=%s", channelName, encodedMessage, token)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "POST" {
			responseWriter.WriteHeader(405)
			return
		}

		if err := writeOkResponse(responseWriter, jsonResponseBody); err != nil {
			panic(err)
		}
	}
}

func WithGroupMemberPostRequestHandler(token string, groupId, userId int, jsonResponseBody []byte) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/usergroups.users.update?token=%s&usergroup=%d&users=%d", token, groupId, userId)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "POST" {
			responseWriter.WriteHeader(405)
			return
		}

		if err := writeOkResponse(responseWriter, jsonResponseBody); err != nil {
			panic(err)
		}
	}
}

func WithUserGetRequestHandler(token string, userId int, jsonResponseBody []byte) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/users.info?token=%s&user=%d", token, userId)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "GET" {
			responseWriter.WriteHeader(405)
			return
		}

		if err := writeOkResponse(responseWriter, jsonResponseBody); err != nil {
			panic(err)
		}
	}
}

func WithGroupMemberGetRequestHandler(token string, groupId int, jsonResponseBody []byte) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/usergroups.users.list?token=%s&usergroup=%d", token, groupId)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "GET" {
			responseWriter.WriteHeader(405)
			return
		}

		if err := writeOkResponse(responseWriter, jsonResponseBody); err != nil {
			panic(err)
		}
	}
}

func WithUsersGetRequestHandler(token string, jsonResponseBody []byte) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/users.list?token=%s", token)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "GET" {
			responseWriter.WriteHeader(405)
			return
		}

		if err := writeOkResponse(responseWriter, jsonResponseBody); err != nil {
			panic(err)
		}
	}
}

func WithGroupsGetRequestHandler(token string, jsonResponseBody []byte) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/usergroups.list?token=%s", token)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "GET" {
			responseWriter.WriteHeader(405)
			return
		}

		if err := writeOkResponse(responseWriter, jsonResponseBody); err != nil {
			panic(err)
		}
	}
}

func WithTokenGetRequestHandler(token string, jsonResponseBody []byte) http.HandlerFunc {
	expectedUri := fmt.Sprintf("/api/auth.test?token=%s", token)
	return func(responseWriter http.ResponseWriter, request *http.Request) {
		if !matchesCommonExpectations(request, expectedUri, responseWriter) {
			return
		}
		if request.Method != "GET" {
			responseWriter.WriteHeader(405)
			return
		}

		if err := writeOkResponse(responseWriter, jsonResponseBody); err != nil {
			panic(err)
		}
	}
}

func writeOkResponse(responseWriter http.ResponseWriter, jsonResponseBody []byte) error {
	responseWriter.WriteHeader(200)
	responseWriter.Header().Set("Content-Type", "application/json")
	_, err := responseWriter.Write(jsonResponseBody)
	return err
}

func matchesCommonExpectations(request *http.Request, expectedUri string, responseWriter http.ResponseWriter) bool {
	if request.RequestURI != expectedUri {
		responseWriter.WriteHeader(404)
		return false
	}
	accept := request.Header.Get("Accept")
	if accept != "application/json" {
		responseWriter.WriteHeader(406)
		return false
	}
	return true
}

