package slack

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)



type FindAllGroupMembersResponse struct {
	Users []User `json:"users"`
}

type GroupMemberRepository interface {
	FindAll(group *Group) ([]User, error)
	Add(group *Group, user *User) error
}

type groupMemberHttpRepository struct {
	serverAddress string
	token         string
	httpClient    *http.Client
}

func NewGroupMemberRepository(serverAddress string, token string) GroupMemberRepository {
	return &groupMemberHttpRepository{
		serverAddress: serverAddress,
		token:         token,
		httpClient:    &http.Client{},
	}
}

func (repo *groupMemberHttpRepository) FindAll(group *Group) ([]User, error) {
	request, err := newJsonRequest("GET", repo.findAllGroupMembersUri(group))
	if err != nil {
		return nil, err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return nil, err
	}
	statusCode := response.StatusCode
	if statusCode != 200 {
		return nil, fmt.Errorf("[Slack client] unable to find users in group %d - server responded with %d (expected 200)", group.Id, statusCode)
	}
	data, err := readAll(response.Body)
	if err != nil {
		return nil, err
	}
	return repo.unmarshallMembers(data)
}

func (repo *groupMemberHttpRepository) Add(group *Group, user *User) error {
	request, err := newJsonRequest("POST", repo.addGroupMemberUri(group, user))
	if err != nil {
		return err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return fmt.Errorf("[Slack client] unable to add user %d to group %d - server responded with %d (expected 200)", user.Id, group.Id, statusCode)
	}
	responseStatus, err := unmarshallResponseStatus(response.Body)
	if err != nil {
		return err
	}
	if !responseStatus.Ok {
		return fmt.Errorf("[Slack client] unable to add user %d to group %d - server responded with 200 but JSON status is not OK", user.Id, group.Id)
	}
	return nil
}

func (repo *groupMemberHttpRepository) findAllGroupMembersUri(group *Group) string {
	queryString := url.Values{
		"token":     []string{repo.token},
		"usergroup": []string{fmt.Sprintf("%d", group.Id)},
	}.Encode()

	return fmt.Sprintf("%s/api/usergroups.users.list?%s", repo.serverAddress, queryString)
}

func (repo *groupMemberHttpRepository) addGroupMemberUri(group *Group, user *User) string {
	queryString := url.Values{
		"token":     []string{repo.token},
		"usergroup": []string{fmt.Sprintf("%d", group.Id)},
		"users":     []string{fmt.Sprintf("%d", user.Id)},
	}.Encode()

	return fmt.Sprintf("%s/api/usergroups.users.update?%s", repo.serverAddress, queryString)
}

func (repo *groupMemberHttpRepository) unmarshallMembers(data []byte) ([]User, error) {
	userResponse := FindAllGroupMembersResponse{}
	err := json.Unmarshal(data, &userResponse)
	if err != nil {
		return nil, err
	}
	return userResponse.Users, nil
}
