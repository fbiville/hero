package slack

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

type Group struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type FindAllGroupsResponse struct {
	Groups []Group `json:"usergroups"`
}

type GroupRepository interface {
	FindAll() ([]Group, error)
}

type groupHttpRepository struct {
	serverAddress string
	token         string
	httpClient    *http.Client
}

func NewGroupRepository(serverAddress string, token string) GroupRepository {
	return &groupHttpRepository{
		serverAddress: serverAddress,
		token:         token,
		httpClient:    &http.Client{},
	}
}

func (repo *groupHttpRepository) FindAll() ([]Group, error) {
	request, err := newJsonRequest("GET", repo.findAllGroupsUri())
	if err != nil {
		return nil, err
	}
	response, err := repo.httpClient.Do(request)
	if err != nil {
		return nil, err
	}

	statusCode := response.StatusCode
	if statusCode != 200 {
		return nil, fmt.Errorf("[Slack client] unable to find all groups - server responded with %d (expected 200)", statusCode)
	}
	data, err := readAll(response.Body)
	if err != nil {
		return nil, err
	}
	return repo.unmarshallGroups(data)
}

func (repo *groupHttpRepository) findAllGroupsUri() string {
	queryString := url.Values{
		"token": []string{repo.token},
	}.Encode()

	return fmt.Sprintf("%s/api/usergroups.list?%s", repo.serverAddress, queryString)
}

func (repo *groupHttpRepository) unmarshallGroups(data []byte) ([]Group, error) {
	groupResponse := FindAllGroupsResponse{}
	err := json.Unmarshal(data, &groupResponse)
	if err != nil {
		return nil, err
	}
	return groupResponse.Groups, nil
}
