package slack

import (
	"net/http"
	"strings"
)

func newJsonRequest(method, uri string) (*http.Request, error) {
	request, err := http.NewRequest(method, uri, strings.NewReader(""))
	if err != nil {
		return nil, err
	}
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Content-Type", "application/json")
	return request, nil
}