# Hero

The hero is a member of a team who proxies requests from other teams and performs running tasks.

## How does it work?

It is a simple Go program called by a [Kubernetes Cron Jobs] running on prod-eu cluster.
It uses Slack API to change the member (hero) of a specific [Slack User Group].
It can also configure an Okta group.
API keys are transmitted by a manually created `hero` [Kubernetes Secret].

It needs to following environment variables to work:

| Name                | Description                                                                     | Example                                                                    |
|---------------------|---------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| SLACK_TOKEN         | Use to call SLACK API. It should have enough permissions to manage user groups  | xoxp-XXXXXXXXXX-XXXXXXXXXXXX-XXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |
| OKTA_TOKEN          | Use to call Okta API. It should have enough permissions to manage user groups   | XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX                                 |
| OKTA_URL            | The Okta workspace                                                              | https://backmarket.okta-emea.com                                           |
| HERO_GROUP_NAME     | The Slack and Okta group name that contains the current user hero               | BoT Ops Hero                                                               |
| ORIGINAL_GROUP_NAME | A group name that contains users that could become hero                         | BoT Ops                                                                    |
| USERS_NAME          | If the ORIGINAL_GROUP_NAME is not provided, use this variable instead           | david,theotime.leveque,quentin.revel                                       |
| SLACK_CHANNEL       | Group that will receive a notification to inform who the new hero is            | bot-ops-private                                                            |


## FAQ

### How can I skip the turn of the Hero of the day?

You just have to re-run the cronjob on production a second time.
In order to do that, switch to the prod EU context and execute the following command: `kubectl create job hero-<override_reason_and_date> --from=cronjob/hero -n default`


[Kubernetes Cron Jobs]: (https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)
[Kubernetes Secret]: (https://kubernetes.io/docs/concepts/configuration/secret/)
[Slack User Group]: (https://get.slack.help/hc/en-us/articles/212906697-Create-a-user-group)
