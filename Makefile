.PHONY: aws-login build build-local push push-latest deploy clean help

NAME = hero
CURRENT_COMMIT = $(shell git rev-parse HEAD)
ECR_REPOSITORY = 154005363564.dkr.ecr.us-east-1.amazonaws.com
BINARY = ./${NAME}
GO_SOURCES = $(shell find . -type f -name '*.go')
GOBIN ?= $(shell go env GOPATH)/bin

.DEFAULT_GOAL := help

aws-login: ## log in to AWS ECR
	$(shell aws ecr get-login --no-include-email --region us-east-1)

kube-config: ## initialize Kubernetes configuration
	mkdir -p ~/.kube && \
	envsubst '\$$KUBE_PROD_EU_CLUSTER_NAME,\$$KUBE_PROD_EU_CERTIFICATE,\$$KUBE_ROLE_ARN' < kube-config-template > ~/.kube/config

build: ## build the project with Docker
	docker build --no-cache -t ${NAME} .

build-local: $(BINARY) ## build the project locally

test: ## run the project tests
	GO111MODULE=on go test -v ./...

install: build ## copy the binary to GOBIN
	cp $(BINARY) $(GOBIN)

$(BINARY): $(GO_SOURCES)
	GO111MODULE=on go build

clean: ## remove the binary
	rm -f $(BINARY)

push: ## tag with current commit and push Docker image
	docker tag ${NAME} ${ECR_REPOSITORY}/${NAME}:${CURRENT_COMMIT}
	docker push ${ECR_REPOSITORY}/${NAME}:${CURRENT_COMMIT}

push-latest: ## tag with latest and push Docker image
	docker tag ${NAME} ${ECR_REPOSITORY}/${NAME}:latest
	docker push ${ECR_REPOSITORY}/${NAME}:latest

deploy: ## deploy to Kubernetes
	kubectl apply -f hero.yaml

# source: http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Print help for each make target
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'